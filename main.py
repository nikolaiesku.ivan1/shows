from copy import copy
from service.persons import Creator, Viewer
from service.shows import Episode, Movie, Series

def main():
    c1 = Creator("Bob", "Creator")
    c2 = Creator("Steve", "Creator")
    v = Viewer("Alan", "Viewer")
    m1 = Movie("Movie1", 2000, [c1], 120)
    m2 = Movie("Movie2", 2009, [c2], 120)

    s1 = Series([
        [Episode(20, "S1E1", 2001, [c1]), Episode(20, "S0E3", 2001, [c1, c2])],
        [Episode(40, "S2E1", 2002, [c1])]],
        "Series1", 2001, [c1, c2])

    print(Series.get_count(), Movie.get_count())
    print(m1.year_diff(2022, 2000))

    print(v.get_viewer_name() + " watched film of " + c1.get_creator_name())
    print(m1*m1)
    m = Movie('name', 2000, [Creator("name", "surname")], 120)
    print(m.descriptor)

    c3 = copy(c2)
    print(c2, c3)


main()
