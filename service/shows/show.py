from abc import ABC, abstractstaticmethod
from dataclasses import dataclass
from typing import List
from service.persons import Creator


@dataclass
class Show(ABC):
    name: str
    year: int
    creators: List[Creator]

    @abstractstaticmethod
    def year_diff(year1: int, year2: int):
        raise NotImplementedError

    def __mul__(self, obj):
        return self.year * obj.year

    def __eq__(self, obj):
        return self.year == obj.year
