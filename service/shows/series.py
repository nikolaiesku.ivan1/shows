from dataclasses import dataclass
from typing import List
from .show import Show
from .episode import Episode


@dataclass
class Series(Show):
    seasons: List[List[Episode]]

    count = 0

    @classmethod
    def get_count(cls) -> None:
        return cls.count

    @staticmethod
    def year_diff(year1: int, year2: int):
        return year1-year2
