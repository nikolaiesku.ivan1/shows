from dataclasses import dataclass
from .show import Show


@dataclass
class Episode(Show):
    length: str

    @staticmethod
    def year_diff(year1: int, year2: int):
        return year1-year2
