from dataclasses import dataclass
from .show import Show


class Descriptor:
    def __init__(self, value=1):
        self.value = value

    def __set__(self, instance, value):
        if value > instance.year:
            raise AttributeError("Bad value!")
        self.value = value

    def __get__(self, instance, owner):
        return self.value


@dataclass
class Movie(Show):
    length: str
    descriptor: Descriptor = Descriptor()

    count = 0

    @classmethod
    def get_count(cls) -> int:
        return cls.count

    @staticmethod
    def year_diff(year1: int, year2: int):
        return year1-year2
