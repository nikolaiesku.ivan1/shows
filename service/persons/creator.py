from dataclasses import dataclass


@dataclass
class Creator():
    name: str
    surname: str

    def get_creator_name(self) -> str:
        return self.name

    def __str__(self):
        return f"Name: {self.name} Surname: {self.surname}"

    def __copy__(self):
        return Creator(self.name, self.surname)
