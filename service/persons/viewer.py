from dataclasses import dataclass


@dataclass
class Viewer():
    name: str
    surname: str

    def get_viewer_name(self) -> str:
        return self.name
